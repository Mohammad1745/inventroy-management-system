@extends('layouts.admin')

@section('title')
    IMS | Home
@endsection

@section('nav-brand')
    Inventory Management System
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Dashboard</h3></div>

                    <div id="content" class="card-body row m-0 col-md-12 col-sm-12">

                        <!-- DOM For Shop Related Statistics Starts -->
                        <div class="card p-0 col-md-8 col-sm-12">

                            <div class="card-header">Shops</div>

                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Total Shops
                                        <span class="badge badge-primary badge-pill">{{$shopAmount}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Total Products
                                        <span class="badge badge-primary badge-pill">{{$productAmount}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Total Product Variety
                                        <span class="badge badge-primary badge-pill">{{$productVariationAmount}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Total Employees
                                        <span class="badge badge-primary badge-pill">{{$employeeAmount}}</span>
                                    </li>
                                </ul>
                                    <button type="button" id="shop-details" class="btn btn-outline-primary mt-2">Details</button>
                            </div>
                        </div>
                        <!-- DOM For Shop Related Statistics Ends -->

                        <!-- DOM For Category & Subcategory Statistics Starts -->
                        <div class="card p-0 col-md-4 col-sm-12">

                            <div class="card-header">Categories & Subcategories</div>

                            <div class="card-body">
                                <div id="category-overview">
                                    <ul class="list-group">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Total Categories
                                            <span id="category-amount" class="badge badge-primary badge-pill">{{$categoryAmount}}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Total Subcategories
                                            <span id="subcategory-amount" class="badge badge-primary badge-pill">{{$subcategoryAmount}}</span>
                                        </li>
                                    </ul>
                                    <a type="button" href="{{route('admin.category.details')}}" id="category-details" class="btn btn-outline-primary mt-2">Details</a>
                                </div>
                             </div>
                        </div>
                        <!-- DOM For Category & Subcategory Statistics Ends -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#content').on('click', '#shop-details', function () {
                alert("Further Instructions Required From Mr. Farhat Shahir For The Action.");
            });
        });
    </script>
@endsection
