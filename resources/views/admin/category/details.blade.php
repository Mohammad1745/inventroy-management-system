@extends('layouts.admin')

@section('title')
    IMS | Categories and Subcategories
@endsection

@section('nav-brand')
    Inventory Management System
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div id="temporary-content" class="card">

                </div>
                <div id="content" class="card">
                    <div class="card-header row m-0 col-md-12">
                        <h3 class="col-md-6">Categories</h3>
                        <span class="col-md-6">
                            <abbr title="Add Category">
                                <i id="category-add-new" class="btn fas fa-plus position-relative float-md-right"></i>
                            </abbr>
                        </span>
                    </div>

                    <div class="card-body row m-0 col-md-12">

                    @if(sizeof($categories)>0)
                        <!-- DOM For Category List Starts -->
                            <div class="card p-0 col-md-5 col-sm-12">

                                <div class="card-header">
                                    Categories
                                </div>
                                <div class="card-body">

                                    <ul id="category-list" class="list-group">
                                        @for($iterator=0; $iterator<sizeof($categories); $iterator++)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                <span id="show-subcategory-{{$categories[$iterator]->id}}" class="btn show-subcategory p-0" data-id="{{$categories[$iterator]->id}}" data-category="{{$categories[$iterator]}}">
                                                    {{$categories[$iterator]->name}}
                                                </span>
                                                <!-- Hidden Dom For Category Edit Button Starts -->
                                                <abbr title="Edit {{$categories[$iterator]->name}}">
                                                    <span id="category-edit-button"  class="btn p-0 hidden-category-info button-{{$categories[$iterator]->id}}" data-toggle="modal" data-target="#category-edit-form" data-category="{{$categories[$iterator]}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </span>
                                                </abbr>
                                                <abbr title="Add Subcategory To {{$categories[$iterator]->name}}">
                                                    <span id="category-add-subcategory-button"  class="btn p-0 hidden-category-info button-{{$categories[$iterator]->id}}" data-category="{{$categories[$iterator]}}">
                                                        <i class="fas fa-plus"></i>
                                                    </span>
                                                </abbr>
                                                <abbr title="Delete {{$categories[$iterator]->name}}">
                                                    <span id="category-delete-button" data-category="{{$categories[$iterator]}}" class="btn pb-0 hidden-category-info  button-{{$categories[$iterator]->id}}" data-toggle="modal" data-target="#category-delete-confirmation" >
                                                        <i class="fas fa-trash-alt"></i>
                                                    </span>
                                                </abbr>
                                                <!-- Hidden Dom For Category Edit Button Ends -->

                                                <span id="category-amount" class="badge badge-primary badge-pill">{{$categories[$iterator]->subcategories->count()}}</span>
                                            </li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                            <!-- DOM For Category List Ends -->

                            <!-- DOM For Subcategory List Starts -->
                            <div class="card p-0 col-md-7 col-sm-12">

                                <div class="card-header">
                                    Subcategories
                                </div>

                                <div id="subcategory-card" class="card-body">

{{--                                    <div id="category-information" style="font-size: 10px;">--}}
{{--                                        --}}{{-- category Category and Subcategory will be shown here after selecting any product form the product list --}}
{{--                                    </div>--}}

                                    <table id="table-subcategory-info">

                                        <tr class="border-bottom p-2">
                                            <th class="">Name</th>
                                            <th></th>
                                            <th></th>
                                        </tr>

                                        @for($iterator=0; $iterator<sizeof($categories); $iterator++)
                                            @foreach($categories[$iterator]->subcategories as $subcategory)
                                                <tr  data-id="{{$categories[$iterator]->id}}"  class="row-{{$categories[$iterator]->id}} table-row">
                                                    <td class="p-1">{{$subcategory->name}}</td>
                                                    <td class="pl-3">
                                                        <abbr title="Edit {{$subcategory->name}}">
                                                            <span id="subcategory-edit-button" data-category="{{$categories[$iterator]}}" data-subcategory="{{$subcategory}}" class="btn pb-0" data-toggle="modal" data-target="#subcategory-edit-form">
                                                                <i class="fa fa-pencil"></i>
                                                            </span>
                                                        </abbr>
                                                    </td>
                                                    <td class="">
                                                        <abbr title="Delete {{$subcategory->name}}">
                                                            <span id="subcategory-delete-button" data-category="{{$categories[$iterator]}}" data-subcategory="{{$subcategory}}" class="btn pb-0" data-toggle="modal" data-target="#subcategory-delete-confirmation" >
                                                                <i class="fas fa-trash-alt"></i>
                                                            </span>
                                                        </abbr>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endfor
                                    </table>

                                </div>

                            </div>
                    @else
                    <!-- If there is no category -->
                        <div class="m-auto"><h4>Empty</h4></div>
                    @endif

                    </div>
                    <!-- DOM For Subcategory List Ends -->

                    <!-- Modal For Category Edit Starts -->
                    <div class="modal fade" id="category-edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="category-name" id="category-name" class="form-control">
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" id="category-edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Category Edit Ends -->

                    <!-- Modal For Subcategory Edit Starts -->
                    <div class="modal fade" id="subcategory-edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="subcategory-name" id="subcategory-name" class="form-control">
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" id="subcategory-edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Subcategory Edit Ends -->

                    <!-- Modal For Category Delete Confirmation Starts -->
                    <div class="modal fade" id="category-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    Sure to delete <span id="delete-category-name"></span>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="form-cancel" class="btn btn-outline-success"  data-dismiss="modal" aria-label="Close">Cancel</button>
                                    <button type="button" id="delete-category-confirmation-button" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Category Delete Confirmation Ends -->

                    <!-- Modal For Subcategory Delete Confirmation Starts -->
                    <div class="modal fade" id="subcategory-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    Sure to delete <span id="delete-subcategory-name"></span>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="form-cancel" class="btn btn-outline-success"  data-dismiss="modal" aria-label="Close">Cancel</button>
                                    <button type="button" id="delete-subcategory-confirmation-button" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Subcategory Delete Confirmation Ends -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        //DOMs with this class need to be hide before the page is ready.
        $('.hidden-category-info').hide();

        $('document').ready(function () {

            //After selecting any category, the corresponding information will be shown
            $('#content').on('click', '.show-subcategory', function () {
                $('.show-subcategory').removeClass('font-weight-bold');
                $(this).addClass('font-weight-bold');

                let id =  $(this).data("id");

                showCategory(id)
            });

            //Category Information are shown on the page
            let showCategory = function(id){
                $('.hidden-category-info').hide('slow');
                $('.table-row').hide();
                $('.button-' + id).show('slow');
                $('#content').find('.row-' + id).show('slow');
            }

            let dynamicDom = $('#content').html();
            let categoryId;
            let categoryName;
            let subcategoryName = [];
            let subcategoryInfoIterator = 0;

            //Clicking the category add new button will trigger the following actions
            $('#content').on('click', '#category-add-new', function () {
                createCategory();
            });

            //Creating a from to add new category
            let createCategory = function () {
                let form =
                    '                <div class="card-header">Enter Category Information</div>\n' +
                    '                <div id="form" class="card-body">\n' +
                    '                    <div id="category-form">\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="name">Name</label>\n' +
                    '                            <input type="text" name="name" id="name" class="form-control">\n' +
                    '                        </div>\n' +
                    '                        <button type="button" id="category-form-submit" class="btn btn-outline-primary">Add</button>\n' +
                    '                        <button type="button" id="form-cancel" class="btn btn-outline-danger">Cancel</button>\n' +
                    '                    </div>\n' +
                    '               </div>';

                $('#content').html(form);
            }

            //Clicking the category form submit button will trigger the following actions
            $('#content').on('click', '#category-form-submit', function () {

                if($('#name').val() < 3) alert("Empty Field: Category Name");
                else{
                    categoryName = $('#name').val();
                    saveCategory();
                }
            });

            //Saving the new data to Database
            let saveCategory = function () {
                $.ajax({
                    url: "{{route('admin.category.store')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'name' : categoryName,
                    }
                }).done(function (response) {
                    console.log(response);
                    categoryId = response.data.id;
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Clicking the category add subcategory button will trigger the following actions
            $('#content').on('click', '#category-add-subcategory-button', function () {
                categoryId = $(this).data('category').id;
                categoryName = $(this).data('category').name;
                appendCategoryInfoAsTemporary();//save
                createSubcategory();
            });

            //The category information are being shown temporarily while subcategoryform is shown
            let appendCategoryInfoAsTemporary = function(){
                let html =
                    '<div class="card-header row m-0 col-md-12">' +
                    '   <div class="col-md-8">' +
                    '       <h6>Category Name: ' + categoryName + '</h6>' +
                    '   </div>' +
                    '   <div class="col-md-4">' +
                    '       <button type="button" id="form-cancel" class="btn btn-outline-danger position-relative float-right">Cancel</button>\n' +
                    '   </div>' +
                    '</div>' ;
                $('#temporary-content').html(html);
            }

            //Subcategory form is being shown for new information
            let createSubcategory = function () {
                let form =
                    '            <div class="card-header">Enter Subcategory Information</div>\n' +
                    '            <div class="row m-0 col-md-12">\n' +
                    '                <div id="form" class="card-body col-md-5">\n' +
                    '                    <div id="subcategory-form">\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="name">Name</label>\n' +
                    '                            <input type="text" name="name" id="name" class="form-control">\n' +
                    '                        </div>\n' +

                    '                        <button type="button" id="category-add-more-subcategory-button" class="btn btn-outline-primary">Add</button>\n' +
                    '                     </div>\n' +
                    '                 </div>\n' +
                    '              <div id="temporary-subcategory-info" class="card-body col-md-7">\n' +
                    '              </div>'+
                    '          </div>';

                $('#content').html(form);
            }

            //Clicking the subcategory form submit button will trigger the following actions
            $('#content').on('click', '#category-add-more-subcategory-button', function () {

                if($('#name').val() < 3) alert("Name must be more than 3 character");
                else{
                    subcategoryName[subcategoryInfoIterator] = $('#name').val();
                    subcategoryInfoIterator++;

                    createSubcategory();
                    showSubcategoryInfoAsTemporary();//save
                }
            });

            //The subcategory information are being shown temporarily while subcategory form is shown
            let showSubcategoryInfoAsTemporary = function(){
                let html =
                    '               <button type="button" id="subcategory-form-submit" class="btn btn-outline-primary position-relative float-right">Save Changes</button>\n' +

                    '               <table id="table-category-info">\n' +
                    '                   <tr class="border-bottom p-2">\n' +
                    '                       <th class="">Name</th>\n' +
                    '                   </tr>\n';

                for(let i=0; i<subcategoryName.length; i++) {
                    html +=
                        '            <tr>\n' +
                        '                <td class="p-1">' + subcategoryName[i] + '</td>\n' +
                        '            </tr>\n';
                }
                html +=     '       </table>';

                $('#temporary-subcategory-info').html(html);
            }

            //Clicking the form submit button will trigger the following actions
            $('#content').on('click', '#subcategory-form-submit', function () {
                if (subcategoryName.length > 0)  saveSubcategory();
                else  alert('No Data Available!');
            });

            //Saving the new data to Database
            let saveSubcategory = function () {
                $.ajax({
                    url: "{{route('admin.subcategory.store')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'categoryId' : categoryId,
                        'subcategoryName' : subcategoryName,
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Category edit section
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#category-edit-button', function () {
                let category = $(this).data('category');
                categoryId = category.id;
                setCategoryEditFormData(category);
            });

            //Feeding data to the edit form
            let setCategoryEditFormData = function(category){
                $('#category-edit-form').find('#category-name').val(category.name);
            }

            //Clicking the submit button will trigger the following actions
            $('#content').on('click', '#category-edit-form-submit', function () {
                categoryName = $('#category-name').val();

                if(categoryName.length < 3) alert('Name must have at least 3 character.');

                else updateCategory();
            });

            //Updating the database with the changes
            let updateCategory = function(){
                $.ajax({
                    url: "{{route('admin.category.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': categoryId,
                        'name': categoryName,
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Subcategory edit section
            let subcategory;
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#subcategory-edit-button', function () {
                categoryId = $(this).data('category').id;
                subcategory = $(this).data('subcategory');
                setSubcategoryEditFromData(subcategory);
            });

            //Feeding data to the edit form
            let setSubcategoryEditFromData = function(subcategory){
                $('#subcategory-edit-form').find('#subcategory-name').val(subcategory.name);
            }

            //Clicking the submit button will trigger the following actions
            $('#content').on('click', '#subcategory-edit-form-submit', function () {
                subcategoryName[0] = $('#subcategory-name').val();

                if(subcategoryName[0].length < 3) alert('Name must have at least 3 character.');
                else updateSubcategory();
            });

            //Updating the database with the changes
            let updateSubcategory = function(){
                $.ajax({
                    url: "{{route('admin.subcategory.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': subcategory.id,
                        'name': subcategoryName[0],
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Category delete section
            //Clicking the delete button will trigger the following actions
            $('#content').on('click', '#category-delete-button', function () {
                let category = $(this).data('category');
                categoryId = category.id;
                $('#content').find('#delete-category-name').html(category.name);
            });

            //Clicking the confirmation button will trigger the following actions
            $('#content').on('click', '#delete-category-confirmation-button', function () {
                deleteCategory(categoryId);
                categoryId = null;
            });

            //Deleting from database
            let deleteCategory = function(id){
                $.ajax({
                    url: "{{route('admin.category.delete')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': id
                    }
                }).done(function (response) {
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Subcategory delete section
            //Clicking the delete button will trigger the following actions
            $('#content').on('click', '#subcategory-delete-button', function () {

                categoryId = $(this).data('category').id;
                subcategory = $(this).data('subcategory');

                $('#content').find('#delete-subcategory-name').html(subcategory.name);
            });

            //Clicking the confirmation button will trigger the following actions
            $('#content').on('click', '#delete-subcategory-confirmation-button', function () {
                deleteSubcategory();
            });

            //Updating the database with the changes
            let deleteSubcategory = function(){
                $.ajax({
                    url: "{{route('admin.subcategory.delete')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id' : subcategory.id
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Refreshing the page for new data
            let refreshPage = function () {
                $.ajax({
                    url: "{{route('admin.category.details')}}",
                    method: 'GET',
                }).done(function (data) {
                    dynamicDom = $("#content", data).html();
                    injectDynamicDom();
                    clearVariables();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Clicking the form cancel button will trigger the following actions
            $('#content').on('click', '#form-cancel', function () {
                injectDynamicDom()
                clearVariables();
            });

            //Clicking the form cancel button will trigger the following actions
            $('#temporary-content').on('click', '#form-cancel', function () {
                clearVariables();
                injectDynamicDom()
            });

            //Injecting a dynamic dom
            let injectDynamicDom = function () {
                setTimeout(function () {
                    $('#temporary-content').html('');
                    $('#content').html(dynamicDom);
                    $('#show-subcategory-' + categoryId).trigger('click');
                }, 200);
            }

            //Clearing All js variables
            let clearVariables = function () {
                categoryName = '';
                subcategoryName = [];
                subcategoryInfoIterator = 0;
            }
        });
    </script>
@endsection
