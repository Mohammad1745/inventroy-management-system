@extends('layouts.shopkeeper')

@section('title')
    {{$shop->name}} | Home
@endsection

@section('nav-brand')
    <span id="nav-brand-name">{{$shop->name}}</span>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div id="content" class="card">
                    <div class="card-header"><h3>Dashboard</h3></div>

                    <div class="card-body row m-0 col-md-12 col-sm-12">

                        <!-- DOM Shop Profile Starts -->
                        <div class="card p-0 col-md-8 col-sm-12">

                            <div class="card-header row m-0 p-2">
                                <span class="col-8 mt-1">Profile</span>
                                <span class="col-4">
                                    <abbr title="Edit Shop Profile">
                                        <span type="button" id="edit-button" data-shop="{{$shop}}" class="btn pb-0 position-relative float-right" data-toggle="modal" data-target="#edit-form">
                                            <i class="fa fa-pencil"></i>
                                        </span>
                                    </abbr>
                                </span>
                            </div>

                            <div class="card-body">
                                <div id="category-overview">
                                    <ul class="list-group">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Shop Name
                                            <span id="shop-name" class=" badge badge-primary badge-pill">{{$shop->name}}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Shop Owner
                                            <span id="shop-user-name" class="badge badge-primary badge-pill">{{$shop->user->name}}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Shop Size
                                            <span id="shop-size" class="badge badge-primary badge-pill">{{$shop->size()}}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Shop Location
                                            <span id="shop-location" class="badge badge-primary badge-pill">{{$shop->location}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- DOM Shop Profile Ends -->

                        <!-- DOM Shop Information Starts -->
                        <div class="card p-0 col-md-4 col-sm-12">

                            <div class="card-header">Shop</div>

                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Products
                                        <span class="badge badge-primary badge-pill">{{$productAmount}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Product Variety
                                        <span class="badge badge-primary badge-pill">{{$productVariationAmount}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Employees
                                        <span class="badge badge-primary badge-pill">{{$employeeAmount}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- DOM Shop Information Ends -->
                    </div>

                    <!-- Modal For Shop Profile Edit Starts -->
                    <div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Profile</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="size">Size</label>
                                        <select name="size" id="size" class="form-control">
                                            @foreach($shop->sizeOptions() as $sizeKey => $sizeOption)
                                            <option value="{{$sizeKey}}">{{$sizeOption}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" name="location" id="location" class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Employee Edit Ends -->

                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {

            let shop = null;
            let name;
            let size;
            let location;
            $('#content').on('click', '#edit-button', function () {
                if(shop == null) shop = $(this).data('shop');
                setFormData();
            });

            let setFormData = function(){
                $('#edit-form').find('#name').val(shop.name);
                $('#edit-form').find('#size').val(shop.size);
                $('#edit-form').find('#location').val(shop.location);
            }


            $('#content').on('click', '#edit-form-submit', function () {
                name = $('#name').val();
                size = $('#size').val();
                location = $('#location').val();

                if(name.length < 3) alert('Name must have at least 3 character.');
                else if(size<1 || size>4)  alert('Enter size currectly');
                else if(location.length < 10) alert('Location must have at least 10 character.');

                else updateShop(name, size, location);
            });

            let updateShop = function(name, size, location){
                $.ajax({
                    url: "{{route('shopkeeper.shop.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': shop.id,
                        'name': name,
                        'size': size,
                        'location': location
                    }
                }).done(function (response) {
                    console.log(response.data);
                    refreshPage(response.data);
                }).fail(function (error) {
                    console.log(error);
                });
            }

            let refreshPage = function(data){
                injectDynamicDom(data);
                clearVariables(data);
            }

            let injectDynamicDom = function(data){
                setTimeout(function () {
                    $('#nav-brand-name').html(data.name);
                    $('#shop-name').html(data.name);
                    $('#shop-size').html(data.sizeInString);
                    $('#shop-location').html(data.location);
                },100);
            }

            $('#content').on('click', '#edit-form-cancel', function () {
                clearVariables();
            });

            let clearVariables = function (data) {
                name = data.name;
                size = data.size;
                location = data.location;
                shop = data;
            }

        });
    </script>
@endsection
