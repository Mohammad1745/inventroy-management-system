@extends('layouts.shopkeeper')

@section('title')
    IMS | Build Shop
@endsection

@section('nav-brand')
    At first you need to build your shop
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header"><h3>Build Shop</h3></div>

                    <div class="card-body">
                        <form action="{{route('shopkeeper.shop.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}">
                                {{$errors->first('name')}}
                            </div>

                            <div class="form-group">
                                <label for="size">Size</label>
                                <select name="size" id="size" class="form-control">
                                    @foreach($shop->sizeOptions() as $sizeKey => $sizeOption)//$shop is empty object, sizeOptions() belongsTo Shop model, it return the size option as key => value patterns
                                        <option value="{{$sizeKey}}" {{$sizeKey == old('size') ? 'selected' : ''}}>{{$sizeOption}}</option>
                                    @endforeach
                                </select>
                                {{$errors->first('size')}}
                            </div>

                            <div class="form-group">
                                <label for="location">Location</label>
                                <input type="text" name="location" id="location" class="form-control" value="{{old('location')}}">
                                {{$errors->first('location')}}
                            </div>

                            <button type="submit" id="form-submit" class="btn btn-primary">Build</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
