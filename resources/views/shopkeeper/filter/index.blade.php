@extends('layouts.shopkeeper')

@section('title')
    {{$shop->name}} | Search Product
@endsection

@section('nav-brand')
    {{$shop->name}}
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div id="content" class="card">
                    <div class="card-header row m-0 col-md-12">
                        <h3 class="col-md-4">Search Product</h3>
                        <span class="col-md-8">

                        </span>
                    </div>

                    <!-- DOM For Search Product Form Starts -->
                    <div class="card-body row m-0 col-md-12">
                        <div id="filter-card" class="card p-0 col-md-4">
                            <div class="card-header row m-0">
                                <h6 class="mt-1 col-5">Filter By</h6>
                                <span class="col-7">
                                    <button type="button" id="search-by-options-button" class="search-button btn btn-primary pt-0 pb-0 position-relative float-right">Search</button>
                                </span>
                            </div>
                            <div class="card-body">
                                <ul id="filter-option-list" class="list-group">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Category
                                        <span type="button" id="category-filter" value="0" class="btn badge badge-primary badge-pill"><i class="fas fa-plus"></i></span>
                                    </li>
                                    <span id="category-options"></span>

                                    <span id="subcategory-filter-area"></span>

                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Quantity
                                        <span type="button" id="quantity-filter" value="0"  class="btn badge badge-primary badge-pill"><i class="fas fa-plus"></i></span>
                                    </li>
                                    <span id="quantity-options"></span>

                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Price
                                        <span type="button" id="price-filter" value="0"  class="btn badge badge-primary badge-pill"><i class="fas fa-plus"></i></span>
                                    </li>
                                    <span id="price-options"></span>
                                </ul>
                            </div>
                        </div>
                        <div id="filter-result-card" class="card col-md-8">
                            <div class="form-group mt-3 row m-0">
                                <input type="text" name="name" id="name" class="form-control col-12" placeholder="Search...">
                                <button type="button" id="search-by-name-button" class="search-button btn btn-primary position-absolute" style="right: 15px; border-top-left-radius: 0; border-bottom-left-radius: 0">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                            <hr>

                            <div id="filter-result">

                            </div>

                        </div>
                    </div>
                    <!-- DOM For Search Product Form Ends -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('document').ready(function () {
            let categories;
            let subcategories;
            let priceSliderValue = [];
            let quantitySliderValue = [];
            let name;
            let selectedCategories = [];
            let selectedSubcategories = [];
            let price = [];
            let quantity = [];

            //Getting all categories and subcategories where the products belongs to
            $.ajax({
                url: "{{route('shopkeeper.filter.initialInformation')}}",
                method: "GET",
            }).done(function (response) {
                categories = response.categories;
                subcategories = response.subcategories;
                priceSliderValue[0] = response.minPrice;
                priceSliderValue[1] = response.maxPrice;
                quantitySliderValue[0] = response.minQuantity;
                quantitySliderValue[1] = response.maxQuantity;
            }).fail(function (error) {
                console.log(error);
            });

            //Category Portion
            //Clicking the category filter button will trigger the following actions
            $('#content').on('click', '#category-filter', function () {
                let selector = $(this);
                handleFilterOptionButton(selector);
                handleCategoryFilterOptions(selector);
            });

            //Show or hide category filter options depending on the previous state
            let handleCategoryFilterOptions = function(selector)
            {
                let value = selector.attr('value');
                if(value == 1)  {
                    let html='';
                    for(let i=0 ; i<categories.length; i++){
                        html +=  '<input type="checkbox" class="ml-3 category-option" id="category' + i + '" name="category" value="' + categories[i].id  + '"> ' + categories[i].name  + '<br>\n';
                    }
                    $('#category-options').html(html);
                }
                else if(value == 0)  {
                    $('#category-options').html('');
                    $('#subcategory-filter-area').html('');
                }
            }

            //Selecting any category checkbox will store the category id and trigger the following actions
            $('#filter-option-list').on('click','.category-option', function () {
                selectedCategories = [];
                $.each($("input[name='category']:checked"), function(){
                    selectedCategories.push($(this).val());
                });
                showSubcategoryFilter();
            });

            //Subcategory Portion
            //Subcategory Filter Menu will be shown
            let showSubcategoryFilter = function()
            {
                if(selectedCategories.length > 0){
                    let html=
                        '   <li class="list-group-item d-flex justify-content-between align-items-center">\n' +
                        '       Subcategory\n' +
                        '       <span type="button" id="subcategory-filter" value="0" class="btn badge badge-primary badge-pill"><i class="fas fa-plus"></i></span>\n' +
                        '   </li>\n' +
                        '   <span id="subcategory-options"></span>';
                    $('#subcategory-filter-area').html(html);
                }
                else{
                    $('#subcategory-filter-area').html('');
                }
            }

            //Clicking the subcategory filter button will trigger the following actions
            $('#content').on('click', '#subcategory-filter', function () {
                let selector = $(this);
                handleFilterOptionButton(selector);
                handleSubcategoryFilterOptions(selector);
            });

            //Show or hide subcategory filter options depending on the previous state and the selected categories
            let handleSubcategoryFilterOptions = function(selector)
            {
                let value = selector.attr('value');
                if(value == 1)  {
                    let html = '';
                    for(let i=0 ; i<selectedCategories.length; i++){
                        for(let j=0; j<subcategories.length; j++){
                            if(subcategories[j].category_id == selectedCategories[i]){
                                html +=  '<input type="checkbox" class="ml-3 subcategory-option" id="subcategory' + j + '" name="subcategory" value="' + subcategories[j].id  + '"> ' + subcategories[j].name  + '<br>\n';
                            }
                        }
                    }
                    $('#subcategory-options').html(html);
                }
                else if(value == 0)  {
                    $('#subcategory-options').html('');
                }
            }

            //Quantity Portion
            //Clicking the quantity filter button will trigger the following actions
            $('#content').on('click', '#quantity-filter', function () {
                let selector = $(this);
                handleQuantityFilterOptions(selector);
                handleFilterOptionButton(selector);
            });

            //Show or hide quantity filter options depending on the previous state, if the options are expanded, a toggle button will be shown
            let handleQuantityFilterOptions = function(selector)
            {
                let value = selector.attr('value');
                if(value == 0)  {
                    let html =
                        '<button type="button" id="quantity-toggle-button" value="0" class="btn btn-primary m-2 pl-1 pr-1 pt-1 pb-1">' +//value 0->fixed, 1->range
                        '   <span id="quantity-fixed" class="p-1 bg-primary text-white">Fixed</span>' +
                        '   <span id="quantity-range" class="p-1 bg-white text-secondary">Range</span>' +
                        '</button> <br>' +

                        '<span id="quantity-field">' +
                        '   <input type="number" id="quantity" name="quantity" class=" form-control mb-2" placeholder="Quantity...">' +
                        '</span>';
                    $('#quantity-options').html(html);
                }
                else if(value == 1)  {
                    $('#quantity-options').html('');
                }
            }

            //Clicking the quanity toggle button will trigger following actions
            $('#filter-option-list').on('click', '#quantity-toggle-button', function () {
                let selector = $(this);
                handleToggleButton(selector);
                toggleCategoryFields(selector);
            });

            //Toggle button state will be changed depending on the previous state
            let handleToggleButton = function(selector)
            {
                let value = selector.attr('value');
                if(value == 0)  {
                    selector.attr('value', '1');
                    selector.find('#quantity-fixed').removeClass('bg-primary text-white').addClass('bg-white text-secondary');
                    selector.find('#quantity-range').removeClass('bg-white text-secondary').addClass('bg-primary text-white');
                }
                else if(value == 1)  {
                    selector.attr('value', '0');
                    selector.find('#quantity-fixed').removeClass('bg-white text-secondary').addClass('bg-primary text-white');
                    selector.find('#quantity-range').removeClass('bg-primary text-white').addClass('bg-white text-secondary');
                }
            }

            //Fixed input field or range slider will be toggle
            let toggleCategoryFields = function(selector)
            {
                let value = selector.attr('value');
                let html = '';
                if(value == 0){
                    html += '<input type="number" id="quantity" name="quantity" class=" form-control mb-2" placeholder="Quantity...">';
                }
                else if(value == 1){
                    html +=
                        '   <div class="container mt-2">\n' +
                        '       <div class="row">\n' +
                        '           <div class="col-sm-12">\n' +
                        '               <div id="quantity-slider-range"></div>\n' +
                        '               </div>\n' +
                        '           </div>\n' +
                        '           <div class="row slider-labels">\n' +
                        '           <div class="col-xs-6 caption ml-2">\n' +
                        '               <strong>Min:</strong> <span id="quantity-slider-range-value1"></span>\n' +
                        '           </div>\n' +
                        '           <div class="col-xs-6 text-right caption ml-5">\n' +
                        '               <strong>Max:</strong> <span id="quantity-slider-range-value2"></span>\n' +
                        '           </div>\n' +
                        '       </div>\n' +
                        '       <div class="row">\n' +
                        '           <div class="col-sm-12">\n' +
                        '               <form>\n' +
                        '                   <input type="hidden" id="min-quantity" name="min-value" value="">\n' +
                        '                   <input type="hidden" id="max-quantity"  name="max-value" value="">\n' +
                        '               </form>\n' +
                        '           </div>\n' +
                        '       </div>\n' +
                        '   </div>';
                }
                $('#quantity-options').find('#quantity-field').html(html);

                activateQuantitySlider(quantitySliderValue);//function of public/js/slider.js
            }


            //Price Portion
            //Clicking the price filter button will trigger the following actions
            $('#content').on('click', '#price-filter', function () {
                let selector = $(this);
                handlePriceFilterOptions(selector);
                handleFilterOptionButton(selector);
            });

            //Show or hide price filter options depending on the previous state, if the options are expanded, a toggle button will be shown
            let handlePriceFilterOptions = function(selector)
            {
                let value = selector.attr('value');
                if(value == 0)  {
                    let html =
                        '<button type="button" id="price-toggle-button" value="0" class="btn btn-primary m-2 pl-1 pr-1 pt-1 pb-1">' +//value 0->fixed, 1->range
                        '   <span id="price-fixed" class="p-1 bg-primary text-white">Fixed</span>' +
                        '   <span id="price-range" class="p-1 bg-white text-secondary">Range</span>' +
                        '</button> <br>' +

                        '<span id="price-field">' +
                        '   <input type="number" id="price" name="price" class=" form-control mb-2" placeholder="price...">' +
                        '</span>';
                    $('#price-options').html(html);
                }
                else if(value == 1)  {
                    $('#price-options').html('');
                }
            }

            //Clicking the price toggle button will trigger following actions
            $('#filter-option-list').on('click', '#price-toggle-button', function () {
                let selector = $(this);
                handlePriceToggleButton(selector);
                togglePriceFields(selector);
            });

            //Toggle button state will be changed depending on the previous state
            let handlePriceToggleButton = function(selector)
            {
                let value = selector.attr('value');
                if(value == 0)  {
                    selector.attr('value', '1');
                    selector.find('#price-fixed').removeClass('bg-primary text-white').addClass('bg-white text-secondary');
                    selector.find('#price-range').removeClass('bg-white text-secondary').addClass('bg-primary text-white');
                }
                else if(value == 1)  {
                    selector.attr('value', '0');
                    selector.find('#price-fixed').removeClass('bg-white text-secondary').addClass('bg-primary text-white');
                    selector.find('#price-range').removeClass('bg-primary text-white').addClass('bg-white text-secondary');
                }
            }

            //Fixed input field or range slider will be toggle
            let togglePriceFields = function(selector)
            {
                let value = selector.attr('value');
                let html = '';
                if(value == 0){
                    html += '<input type="number" id="price" name="price" class=" form-control mb-2" placeholder="Price...">';
                }
                else if(value == 1){
                    html +=
                        '   <div class="container mt-2">\n' +
                        '       <div class="row">\n' +
                        '           <div class="col-sm-12">\n' +
                        '               <div id="price-slider-range"></div>\n' +
                        '               </div>\n' +
                        '           </div>\n' +
                        '           <div class="row slider-labels">\n' +
                        '           <div class="col-xs-6 caption ml-2">\n' +
                        '               <strong>Min:</strong> <span id="price-slider-range-value1"></span>\n' +
                        '           </div>\n' +
                        '           <div class="col-xs-6 text-right caption ml-5">\n' +
                        '               <strong>Max:</strong>    <span id="price-slider-range-value2"></span>\n' +
                        '           </div>\n' +
                        '       </div>\n' +
                        '       <div class="row">\n' +
                        '           <div class="col-sm-12">\n' +
                        '               <form>\n' +
                        '                   <input type="hidden" id="min-price" name="min-value" value="">\n' +
                        '                   <input type="hidden" id="max-price"  name="max-value" value="">\n' +
                        '               </form>\n' +
                        '           </div>\n' +
                        '       </div>\n' +
                        '   </div>';
                }
                $('#price-options').find('#price-field').html(html);

                activatePriceSlider(priceSliderValue);//function of public/js/slider.js
            }

            //
            let handleFilterOptionButton = function(selector)
            {
                let value = selector.attr('value');
                if(value == 0)  {
                    selector.attr('value', '1');
                    selector.find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
                }
                else if(value == 1)  {
                    selector.attr('value', '0');
                    selector.find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
                }
            }

            //Search by name portion
            //Clicking the search by name button will trigger the following actions
            $('#content').on('click','#search-by-name-button', function () {
                name = $('#name').val();

                if(name != ''){
                    searchProductsByName();
                    $('#filter-option-list').find('.fa-minus').trigger('click'); //Reset Filter options
                }
                else{
                    $('#filter-result').html('Empty Field');
                }
            });

            //Searching to database
            let searchProductsByName = function () {
                $.ajax({
                    url: "{{route('shopkeeper.filterByName')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'name': name,
                    }
                }).done(function (response) {
                    console.log(response);
                    showResult(response);
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Search by options portion
            //Clicking the search by option button will trigger the following actions
            $('#content').on('click','#search-by-options-button', function () {
                let response = getFilterOptionValues();

                if(response == true){
                    searchProductsByOptions();
                    $('#name').val('');//Make search bar empty
                }
                else{
                    $('#filter-result').html('Filter Criteria Not Selected');
                }
            });

            let getFilterOptionValues = function () {
                //value of selected categories
                selectedCategories = [];
                $.each($("input[name='category']:checked"), function(){
                    selectedCategories.push($(this).val());
                });

                //value of selected subcategories
                selectedSubcategories = [];
                $.each($("input[name='subcategory']:checked"), function(){
                    selectedSubcategories.push($(this).val());
                });
                //value of quantity
                quantity[0] = $('#quantity-options').find('#quantity').val();
                if(quantity[0] == null){
                    quantity[0] = $('#quantity-options').find('#quantity-slider-range-value1').html();
                    quantity[1] = $('#quantity-options').find('#quantity-slider-range-value2').html();
                }
                else{
                    quantity[1] = $('#quantity-options').find('#quantity').val();
                }
                //value of price
                price[0] = $('#price-options').find('#price').val();
                if(price[0] == null){
                    price[0] = $('#price-options').find('#price-slider-range-value1').html();
                    price[1] = $('#price-options').find('#price-slider-range-value2').html();
                }
                else{
                    price[1] = $('#price-options').find('#price').val();
                }

                if(selectedCategories.length == 0 && quantity[1] == null && price[1] == null){
                    return false;
                }
                else{
                    if(quantity[1] == null) quantity = quantitySliderValue;
                    if(price[1] == null) price = priceSliderValue;
                    return true;
                }
            }

            let searchProductsByOptions = function () {
                $.ajax({
                    url: "{{route('shopkeeper.filterByOptions')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'categoryIds': selectedCategories,
                        'subcategoryIds': selectedSubcategories,
                        'quantity': quantity,
                        'price': price,
                    }
                }).done(function (response) {
                    console.log(response);
                    showResult(response);
                    clearVariables();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            let showResult = function (response) {
                let html = '';
                if(response.success){
                    let productVariations = response.data;
                    html +=
                        '   <table class="filter-result-table">\n' +
                        '      <tr class="border-bottom p-2">\n' +
                        '         <th class="">Item</th>\n' +
                        '         <th class="pl-3">Product</th>\n' +
                        '         <th class="pl-3">Quantity</th>\n' +
                        '         <th class="pl-3">Price</th>\n' +
                        '         <th class="pl-3">Availability</th>\n' +
                        '     </tr>\n' ;

                    for(let i=0; i<productVariations.length; i++){
                        html += ' <tr>\n' +
                            '     <td>' + productVariations[i].name + '</td>\n'+
                            '     <td class="pl-3">' + productVariations[i].productName + '</td>\n'+
                            '     <td class="pl-3">' + productVariations[i].quantity + '</td>\n' +
                            '     <td class="pl-3">' + productVariations[i].price + '</td>\n' +
                            '     <td class="pl-3">';

                        if(productVariations[i].quantity > 0){
                            html += '<span class="badge badge-pill badge-success text-success ml-3">.</span>\n';
                        }
                        else{
                            html += '<span class="badge badge-pill badge-danger text-danger ml-3">.</span>\n';
                        }
                    }
                    html += '       </td>\n' +
                            '     </tr>\n' +
                            '  </table>';
                }
                else{
                    html += response.message;
                }

                $('#filter-result').html(html);
            }
            let clearVariables = function () {
                name = null;
                selectedSubcategories = [];
                price = [];
                quantity = [];
            }
        });
    </script>
@endsection
