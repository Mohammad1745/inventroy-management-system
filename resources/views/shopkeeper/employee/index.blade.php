@extends('layouts.shopkeeper')

@section('title')
    {{$shop->name}} | Employees
@endsection

@section('nav-brand')
    {{$shop->name}}
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div id="content" class="card">

                    <div class="card-header row m-0 col-md-12">

                        <h3 class="col-md-4">Employees</h3>

                        <span class="col-md-8">
                            <abbr title="Add Employee">
                                <span type="button" id="create-button" class="btn pb-0 position-relative float-md-right" data-toggle="modal" data-target="#create-form">
                                <i class="fas fa-plus"></i>
                                </span>
                            </abbr>
                        </span>

                    </div>

                    <!-- DOM For Employee List Starts -->
                    <div class="card-body row m-0">
                        @if(sizeof($employees)>0)
                            <table>
                                <tr>
                                    <th>Name</th>
                                    <th class="pl-3">Salary</th>
                                    <th class="pl-3">Working</th>
                                    <th class="pl-3">Started</th>
                                    <th class="pl-3">Ended</th>
                                    <th class="pl-3"></th>
                                    <th class="pl-3"></th>
                                </tr>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td >{{$employee->name}}</td>
                                        <td class="pl-3">{{$employee->salary}}tk</td>
                                        <td class="pl-3">
                                            @if($employee->still_working)
                                                <span class="badge badge-pill badge-success text-success ml-3">.</span>
                                            @else
                                                <span class="badge badge-pill badge-danger text-danger ml-3">.</span>
                                            @endif
                                        </td>
                                        <td class="pl-3">{{$employee->started_at}}</td>
                                        <td class="pl-3">{{$employee->ended_at == null ? '--------' : $employee->ended_at}}</td>
                                        <td class="pl-3">
                                            <abbr title="Edit {{$employee->name}}'s Information">
                                                <span id="edit-button" data-employee="{{$employee}}" class="btn pb-0" data-toggle="modal" data-target="#edit-form">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                            </abbr>
                                        </td>
                                        <td>
                                            <abbr title="Delete {{$employee->name}}">
                                                <span id="delete-button" data-id="{{$employee->id}}" class="btn pb-0">
                                                    <i class="fas fa-trash-alt"></i>
                                                </span>
                                            </abbr>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>
                        @else
                            <!-- If there is no employee -->
                            <div class="m-auto"><h4>Empty</h4></div>
                        @endif
                    </div>
                    <!-- DOM For Employee List Ends -->

                    <!-- Modal For Employee Create Starts -->
                    <div class="modal fade" id="create-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Enter Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="create-name" id="create-name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="salary">Salary</label>
                                        <input type="number" name="create-salary" id="create-salary" class="form-control" min="10" max="99999999">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="create-form-submit" class="btn btn-outline-primary" data-dismiss="modal" aria-label="Close">Save</button>
                                    <button type="button" id="create-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Employee Create Ends -->

                    <!-- Modal For Employee Edit Starts -->
                    <div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="salary">Salary</label>
                                        <input type="number" name="salary" id="salary" class="form-control" min="10" max="99999999">
                                    </div>

                                    <div class="form-group">
                                        <label for="started">Started</label>
                                        <input type="date" name="started" id="started" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="ended">Ended</label>
                                        <input type="date" name="ended" id="ended" class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Employee Edit Ends -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('document').ready(function () {

            let dynamicDom = $('#content').html();

            let name = '';
            let salary = 0;
            let started ;
            let ended ;

            //Clicking the create form submit button will trigger the following actions
            $('#content').on('click', '#create-form-submit', function () {
                name = $('#create-name').val();
                salary = $('#create-salary').val();

                if(name.length < 3) alert('Name must have at least 3 character.');
                else if(salary == 0) alert('Empty Field: Salary');
                else saveEmployee();
            });

            //Saving the new data to Database
            let saveEmployee = function(){
                $.ajax({
                    url: "{{route('shopkeeper.employee.store')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'name': name,
                        'salary': salary
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Employee edit section
            let employee;
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#edit-button', function () {
                employee = $(this).data('employee');
                setFormData();
            });

            //Feeding data to the edit form
            let setFormData = function(){
                $('#edit-form').find('#name').val(employee.name);
                $('#edit-form').find('#salary').val(employee.salary);
                $('#edit-form').find('#started').val(employee.started_at);
                $('#edit-form').find('#ended').val(employee.ended_at);
                $('#edit-form').find('#ended').attr('min', employee.started_at);
            }

            //Clicking the submit button will trigger the following actions
            $('#content').on('click', '#edit-form-submit', function () {
                name = $('#name').val();
                salary = $('#salary').val();
                started = $('#started').val();
                ended = $('#ended').val();

                if(name.length < 3) alert('Name must have at least 3 character.');
                else if(salary == '')  alert('Empty Field: Salary');
                else if(started == '') alert('Empty Field: Started');

                else updateEmployee();
            });

            //Updating the database with the changes
            let updateEmployee = function(){
                $.ajax({
                    url: "{{route('shopkeeper.employee.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': employee.id,
                        'name': name,
                        'salary': salary,
                        'started': started,
                        'ended': ended
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Employee delete section
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#delete-button', function () {
                let id = $(this).data('id');
                //console.log('delete ' + id);
                deleteEmployee(id);
            });

            //Updating the database with the changes
            let deleteEmployee = function(id){
                $.ajax({
                    url: "{{route('shopkeeper.employee.delete')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': id
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Refreshing the page for new data
            let refreshPage = function(){
                $.ajax({
                    url: "{{route('shopkeeper.employee.index')}}",
                    method: 'GET',
                }).done(function (data) {
                    injectDynamicDom(data);
                    clearVariables()
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Injecting a dynamic dom
            let injectDynamicDom = function(data){
                dynamicDom = $("#content", data).html();
                setTimeout(function () {
                    $('#content').html(dynamicDom);
                },100);;
            }

            //Clicking the form cancel button will trigger the following actions
            $('#content').on('click', '#create-form-cancel', function () {
                clearVariables();
            });

            //Clicking the form cancel button will trigger the following actions
             $('#content').on('click', '#edit-form-cancel', function () {
                 clearVariables();
             });

            //Clearing All js variables
            let clearVariables = function () {
                name = '';
                salary = 0;
            }
        });
    </script>
@endsection
