@extends('layouts.shopkeeper')

@section('title')
    {{$shop->name}} | Products
@endsection

@section('nav-brand')
    {{$shop->name}}
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div id="temporary-content" class="card">

                </div>
                <div id="content" class="card">
                    <div class="card-header row m-0 col-md-12">
                        <h3 class="col-md-4">Products</h3>
                        <span class="col-md-8">
                            <abbr title="Add Product">
                                <i id="product-add-new" class="btn fas fa-plus position-relative float-md-right"></i>
                            </abbr>
                        </span>
                    </div>

                    <div class="card-body row m-0 col-md-12">

                        @if(sizeof($products)>0)
                            <!-- DOM For Product List Starts -->
                            <div class="card p-0 col-md-4 col-sm-12">

                                <div class="card-body">

                                    <ul id="product-list" class="list-group">
                                        @for($iterator=0; $iterator<sizeof($products); $iterator++)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                <span id="show-variation-{{$products[$iterator]->id}}" class="btn show-variation p-0" data-id="{{$products[$iterator]->id}}" data-category="{{$products[$iterator]->category}}" data-subcategory="{{$products[$iterator]->subcategory}}">
                                                    {{$products[$iterator]->name}}
                                                </span>
                                                <!-- Hidden Dom For Product Edit Button Starts -->
                                                <abbr title="Edit {{$products[$iterator]->name}}">
                                                    <span id="product-edit-button"  class="btn p-0 hidden-product-info button-{{$products[$iterator]->id}}" data-toggle="modal" data-target="#product-edit-form" data-product="{{$products[$iterator]}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </span>
                                                </abbr>
                                                <abbr title="Add Variation To {{$products[$iterator]->name}}">
                                                    <span id="product-add-variation-button"  class="btn p-0 hidden-product-info button-{{$products[$iterator]->id}}" data-product="{{$products[$iterator]}}"  data-category="{{$products[$iterator]->category}}" data-subcategory="{{$products[$iterator]->subcategory}}">
                                                        <i class="fas fa-plus"></i>
                                                    </span>
                                                </abbr>
                                                <abbr title="Delete {{$products[$iterator]->name}}">
                                                    <span id="product-delete-button" data-product="{{$products[$iterator]}}" class="btn pb-0 hidden-product-info  button-{{$products[$iterator]->id}}" data-toggle="modal" data-target="#product-delete-confirmation" >
                                                        <i class="fas fa-trash-alt"></i>
                                                    </span>
                                                </abbr>
                                                <!-- Hidden Dom For Product Edit Button Ends -->

                                                <span id="category-amount" class="badge badge-primary badge-pill">{{$products[$iterator]->productVariations->count()}}</span>
                                            </li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                            <!-- DOM For Product List Ends -->

                            <!-- DOM For Product Variation List Starts -->
                            <div class="card p-0 col-md-8 col-sm-12">

                                <div id="product-variation-card" class="card-body">

                                    <div id="product-information" style="font-size: 10px;">
                                        {{-- Product Category and Subcategory will be shown here after selecting any product form the product list --}}
                                    </div>

                                    <table id="table-product-variation-info">

                                        <tr class="border-bottom p-2">
                                            <th class="">Item</th>
                                            <th class="pl-3">Quantity</th>
                                            <th class="pl-3">Price</th>
                                            <th class="pl-3">Availability</th>
                                            <th class="pl-3"></th>
                                            <th></th>
                                        </tr>

                                        @for($iterator=0; $iterator<sizeof($products); $iterator++)
                                            @foreach($products[$iterator]->productVariations as $productVariation)
                                                <tr  data-id="{{$products[$iterator]->id}}"  class="row-{{$products[$iterator]->id}} table-row">
                                                    <td class="p-1">{{$productVariation->name}}</td>
                                                    <td class="pl-3">{{$productVariation->quantity}}</td>
                                                    <td class="pl-3">{{$productVariation->price}} tk</td>
                                                    <td class="pl-3">
                                                        @if($productVariation->status==ACTIVE)
                                                            <span class="badge badge-pill badge-success text-success ml-3">.</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger text-danger ml-3">.</span>
                                                        @endif
                                                    </td>
                                                    <td class="pl-3">
                                                        <abbr title="Edit {{$productVariation->name}}">
                                                            <span id="product-variation-edit-button" data-product="{{$products[$iterator]}}" data-variation="{{$productVariation}}" class="btn pb-0" data-toggle="modal" data-target="#variation-edit-form">
                                                                <i class="fa fa-pencil"></i>
                                                            </span>
                                                        </abbr>
                                                    </td>
                                                    <td class="">
                                                        <abbr title="Delete {{$productVariation->name}}">
                                                            <span id="product-variation-delete-button" data-product="{{$products[$iterator]}}" data-variation="{{$productVariation}}" class="btn pb-0" data-toggle="modal" data-target="#product-variation-delete-confirmation" >
                                                                <i class="fas fa-trash-alt"></i>
                                                            </span>
                                                        </abbr>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endfor
                                    </table>

                                </div>

                            </div>
                        @else
                            <!-- If there is no product -->
                            <div class="m-auto"><h4>Empty</h4></div>
                        @endif

                    </div>
                    <!-- DOM For Product Variation List Ends -->

                    <!-- Modal For Product Edit Starts -->
                    <div class="modal fade" id="product-edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="product-name">Name</label>
                                        <input type="text" name="product-name" id="product-name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="category">category</label>
                                        <select name="category" id="category" class="form-control">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" name="{{$category->name}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="subcategory">Subcategory</label>
                                        <select name="subcategory" id="subcategory" class="form-control">
                                            @foreach($categories as $category)
                                                @foreach($category->subcategories as $subcategory)
                                                    <option class="subcategory-option category-{{$category->id}}" data-id="{{$category->id}}" value="{{$subcategory->id}}" name="{{$subcategory->name}}">{{$subcategory->name}}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" id="product-edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Product Edit Ends -->

                    <!-- Modal For Product Variation Edit Starts -->
                    <div class="modal fade" id="variation-edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="variation-name" id="variation-name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="number" name="variation-price" id="variation-price" class="form-control" max="99999999" min="0">
                                    </div>

                                    <div class="form-group">
                                        <label for="quantity">Quantity</label>
                                        <input type="number" name="variation-quantity" id="variation-quantity" class="form-control" min="0" max="99999999">
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" id="variation-edit-form-submit" class="btn btn-outline-primary"  data-dismiss="modal" aria-label="Close">Save changes</button>
                                    <button type="button" id="edit-form-cancel" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Product Variation Edit Ends -->

                    <!-- Modal For Product Delete Confirmation Starts -->
                    <div class="modal fade" id="product-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    Sure to delete <span id="delete-product-name"></span>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="form-cancel" class="btn btn-outline-success"  data-dismiss="modal" aria-label="Close">Cancel</button>
                                    <button type="button" id="delete-product-confirmation-button" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Product Delete Confirmation Ends -->

                    <!-- Modal For Product Variation Delete Confirmation Starts -->
                    <div class="modal fade" id="product-variation-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    Sure to delete <span id="delete-product-variation-name"></span>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="form-cancel" class="btn btn-outline-success"  data-dismiss="modal" aria-label="Close">Cancel</button>
                                    <button type="button" id="delete-product-variation-confirmation-button" class="btn btn-outline-danger"  data-dismiss="modal" aria-label="Close">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal For Product variation Delete Confirmation Ends -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        //DOMs with this class need to be hide before the page is ready.
        $('.hidden-product-info').hide();

        $('document').ready(function () {

            //After selecting any product, the corresponding information will be shown
            $('#content').on('click', '.show-variation', function () {
                $('.show-variation').removeClass('font-weight-bold');
                $(this).addClass('font-weight-bold');

                let id =  $(this).data("id");
                let category = $(this).data('category');
                let subcategory = $(this).data('subcategory');

                showProduct(id, category, subcategory)
            });

            //Product Information are shown on the page
            let showProduct = function(id, category, subcategory){
                $('.hidden-product-info').hide('slow');
                $('.table-row').hide();
                $('.button-' + id).show('slow');
                $('#content').find('.row-' + id).show('slow');

                let html =
                    'Category: ' + category.name + '<br>\n' +
                    'Subcategory: ' + subcategory.name + '<br>';

                $('#product-information').html(html);
            }

            let dynamicDom = $('#content').html();
            let productId;
            let productName;
            let productCategory;
            let productSubcategory;
            let productCategoryId;
            let productSubcategoryId;
            let productVariationName = [];
            let productVariationQuantity = [0];
            let productVariationPrice = [0];
            let productVariationInfoIterator = 0;

            //Clicking the product add new button will trigger the following actions
            $('#content').on('click', '#product-add-new', function () {
                createProduct();
            });

            //Creating a from to add new product
            let createProduct = function () {
                let form =
                    '                <div class="card-header">Enter Product Information</div>\n' +
                    '                <div id="form" class="card-body">\n' +
                    '                    <div id="product-form">\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="name">Name</label>\n' +
                    '                            <input type="text" name="name" id="name" class="form-control">\n' +
                    '                        </div>\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="category">category</label>\n' +
                    '                            <select name="category" id="category" class="form-control">\n' +
                    '                                <option value="" disabled selected>Select Product Category</option>\n' +
                    '                                @foreach($categories as $category)\n' +
                    '                                    <option value="{{$category->id}}" name="{{$category->name}}">{{$category->name}}</option>\n' +
                    '                                @endforeach\n' +
                    '                            </select>\n' +
                    '                        </div>\n' +
                    '                        <div id="field-subcategory" class="form-group">\n' +
                    '                            <label for="subcategory">Subcategory</label>\n' +
                    '                            <select name="subcategory" id="subcategory" class="form-control">\n' +
                    '                                <option value="" disabled selected>Select Product Subcategory</option>\n' +
                    '                                @foreach($categories as $category)\n' +
                    '                                    @foreach($category->subcategories as $subcategory)\n' +
                    '                                       <option class="subcategory-option category-{{$category->id}}" data-id="{{$category->id}}" value="{{$subcategory->id}}" name="{{$subcategory->name}}">{{$subcategory->name}}</option>\n' +
                    '                                    @endforeach\n' +
                    '                                @endforeach\n' +
                    '                            </select>\n' +
                    '                        </div>\n' +
                    '                        <button type="button" id="product-form-submit" class="btn btn-outline-primary">Add</button>\n' +
                    '                        <button type="button" id="form-cancel" class="btn btn-outline-danger">Cancel</button>\n' +
                    '                    </div>\n' +
                    '               </div>';

                $('#content').html(form);
                $('#content').find('#field-subcategory').hide();//subcategory field is hidden.
            }

            //After selecting any category the corresponding subcategories will be shown on the subcategory option in form
            $('#content').on('click', '#category', function () {
                let categoryId = $(this).val();
                $('#content').find('#field-subcategory').show('slow'); //subcategory field is shown
                $('#content').find('.subcategory-option').hide();      //all options are hidden
                $('#content').find('.category-' + categoryId).show();  //showing the subcategories related to the category selected
            });


            //Clicking the product form submit button will trigger the following actions
            $('#content').on('click', '#product-form-submit', function () {

                if($('#name').val() < 3) alert("Empty Field: Product Name");
                else if($('#category').val() == null) alert("Empty Field: Product Category");
                else if($('#subcategory').val() == null) alert("Empty Field: Product Subcategory");
                else{
                    productName = $('#name').val();
                    productCategoryId = $('#category').val();
                    productSubcategoryId = $('#subcategory').val();

                    saveProduct();
                }
            });

            //Saving the new data to Database
            let saveProduct = function () {
                $.ajax({
                    url: "{{route('shopkeeper.product.store')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'productName' : productName,
                        'productCategoryId' : productCategoryId,
                        'productSubcategoryId' : productSubcategoryId,
                    }
                }).done(function (response) {
                    console.log(response);
                    productId = response.data.id;
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Clicking the product add variation button will trigger the following actions
            $('#content').on('click', '#product-add-variation-button', function () {
                productId = $(this).data('product').id;
                productName = $(this).data('product').name;
                productCategory = $(this).data('category').name;
                productSubcategory = $(this).data('subcategory').name;
                appendProductInfoAsTemporary();//save
                createProductVariation();
            });

            //The product information are being shown temporarily while product variation form is shown
            let appendProductInfoAsTemporary = function(){
                let html =
                    '<div class="card-header row m-0 col-md-12">' +
                    '   <div class="col-md-8">' +
                    '       <h6>Product Name: ' + productName + '</h6>' +
                    '       <h6>Product Category: ' + productCategory + '</h6>' +
                    '       <h6>Product Subcategory: ' + productSubcategory + '</h6>' +
                    '   </div>' +
                    '   <div class="col-md-4">' +
                    '       <button type="button" id="form-cancel" class="btn btn-outline-danger position-relative float-right">Cancel</button>\n' +
                    '   </div>' +
                    '</div>' ;
                $('#temporary-content').html(html);
            }

            //Product variation form is being shown for new information
            let createProductVariation = function () {
                let form =
                    '            <div class="card-header">Enter Product Variation Information</div>\n' +
                    '            <div class="row m-0 col-md-12">\n' +
                    '                <div id="form" class="card-body col-md-5">\n' +
                    '                    <div id="product-variation-form">\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="name">Name</label>\n' +
                    '                            <input type="text" name="name" id="name" class="form-control">\n' +
                    '                        </div>\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="price">Price</label>\n' +
                    '                            <input type="number" name="price" id="price" class="form-control" max="99999999" min="0">\n' +
                    '                        </div>\n' +

                    '                        <div class="form-group">\n' +
                    '                            <label for="quantity">Quantity</label>\n' +
                    '                            <input type="number" name="quantity" id="quantity" class="form-control" max="99999999" min="0">\n' +
                    '                        </div>\n' +

                    '                        <button type="button" id="product-add-more-variation-button" class="btn btn-outline-primary">Add</button>\n' +
                    '                     </div>\n' +
                    '                 </div>\n' +
                    '              <div id="temporary-product-variation-info" class="card-body col-md-7">\n' +
                    '              </div>'+
                    '          </div>';

                $('#content').html(form);
            }

            //Clicking the product variation form submit button will trigger the following actions
            $('#content').on('click', '#product-add-more-variation-button', function () {

                if($('#name').val() == '') alert("Empty Field: Name");
                else if($('#quantity').val() == '') alert("Empty Field: Quantity");
                else if($('#price').val() == '') alert("Empty Field: Price");
                else{
                    productVariationName[productVariationInfoIterator] = $('#name').val();
                    productVariationQuantity[productVariationInfoIterator] = $('#quantity').val();
                    productVariationPrice[productVariationInfoIterator] = $('#price').val();
                    productVariationInfoIterator++;

                    createProductVariation();
                    showProductVariationInfoAsTemporary();//save
                }
            });

            //The product variation information are being shown temporarily while product variation form is shown
            let showProductVariationInfoAsTemporary = function(){
                let html =
                    '               <button type="button" id="product-variation-form-submit" class="btn btn-outline-primary position-relative float-right">Save Changes</button>\n' +

                    '               <table id="table-category-info">\n' +
                    '                   <tr class="border-bottom p-2">\n' +
                    '                       <th class="">Item</th>\n' +
                    '                       <th class="pl-3">Quantity</th>\n' +
                    '                       <th class="pl-3">Price</th>\n' +
                    '                   </tr>\n';

                    for(let i=0; i<productVariationName.length; i++) {
                        html +=
                            '            <tr>\n' +
                            '                <td class="p-1">' + productVariationName[i] + '</td>\n' +
                            '                <td class="pl-3">' + productVariationQuantity[i] + '</td>\n' +
                            '                <td class="pl-3">' + productVariationPrice[i] + 'tk</td>\n' +
                            '            </tr>\n';
                    }
                html +=     '       </table>';

                $('#temporary-product-variation-info').html(html);
            }

            //Clicking the form submit button will trigger the following actions
            $('#content').on('click', '#product-variation-form-submit', function () {
                if (productVariationName.length > 0)  saveProductVariation();
                else  alert('No Data Available!');
            });

            //Saving the new data to Database
            let saveProductVariation = function () {
                $.ajax({
                    url: "{{route('shopkeeper.productVariation.store')}}",
                    method: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        'productId' : productId,
                        'productVariationName' : productVariationName,
                        'productVariationQuantity' : productVariationQuantity,
                        'productVariationPrice' : productVariationPrice,
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Product edit section
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#product-edit-button', function () {
                let product = $(this).data('product');
                productId = product.id;
                setProductEditFormData(product);
            });

            //Feeding data to the edit form
            let setProductEditFormData = function(product){
                $('#product-edit-form').find('#product-name').val(product.name);
                $('#product-edit-form').find('#category').val(product.category_id);
                $('#product-edit-form').find('#subcategory').val(product.subcategory_id);
            }

            //After selecting any category the correspondent subcategories will be shown on the subcategory option in form
            $('#content').on('click', '#category', function () {
                let categoryId = $(this).val();
                $('#content').find('#field-subcategory').show('slow'); //subcategory field is shown
                $('#content').find('.subcategory-option').hide();      //all options are hidden
                $('#content').find('.category-' + categoryId).show();  //showing the subcategories related to the category selected

                $('#content').find('#subcategory').val(function () {   //the first option is selected automatically
                    return $('#content').find('.category-' + categoryId).first().val();
                });
            });

            //After selecting any subcategory the correspondent category will be selected automatically in edit form
            $('#content').on('click', '#subcategory', function () {
                let categoryId = $(this).children('option:selected').data('id');
                $('#content').find('#category').val(categoryId);
            });

            //Clicking the submit button will trigger the following actions
            $('#content').on('click', '#product-edit-form-submit', function () {
                productName = $('#product-name').val();
                productCategory = $('#category').val();
                productSubcategory = $('#subcategory').val();

                if(productName.length < 3) alert('Name must have at least 3 character.');
                else if(productCategory == null)  alert('Category Not Selected');
                else if(productSubcategory == null) alert('Subcategory Not Selected');

                else updateProduct();
            });

            //Updating the database with the changes
            let updateProduct = function(){
                $.ajax({
                    url: "{{route('shopkeeper.product.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': productId,
                        'name': productName,
                        'categoryId': productCategory,
                        'subcategoryId': productSubcategory,
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Product Variation edit section
            let productVariation;
            //Clicking the edit button will trigger the following actions
            $('#content').on('click', '#product-variation-edit-button', function () {
                productId = $(this).data('product').id;
                productVariation = $(this).data('variation');

                setProductVariationEditFromData(productVariation);
            });

            //Feeding data to the edit form
            let setProductVariationEditFromData = function(productVariation){
                $('#variation-edit-form').find('#variation-name').val(productVariation.name);
                $('#variation-edit-form').find('#variation-price').val(productVariation.price);
                $('#variation-edit-form').find('#variation-quantity').val(productVariation.quantity);
            }

            //Clicking the submit button will trigger the following actions
            $('#content').on('click', '#variation-edit-form-submit', function () {
                productVariationName[0] = $('#variation-name').val();
                productVariationPrice[0] = $('#variation-price').val();
                productVariationQuantity[0] = $('#variation-quantity').val();

                if(productVariationName[0].length < 3) alert('Name must have at least 3 character.');
                else if(productVariationPrice[0] == '')  alert('Empty Field: Price');
                else if(productVariationQuantity[0] == '') alert('Empty Field: Quantity');

                else updateProductVariation();
            });

            //Updating the database with the changes
            let updateProductVariation = function(){
                $.ajax({
                    url: "{{route('shopkeeper.productVariation.update')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': productVariation.id,
                        'name': productVariationName[0],
                        'price': productVariationPrice[0],
                        'quantity': productVariationQuantity[0],
                    }
                }).done(function (response) {
                    //console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Product delete section
            //Clicking the delete button will trigger the following actions
            $('#content').on('click', '#product-delete-button', function () {
                let product = $(this).data('product');
                productId = product.id;
                $('#content').find('#delete-product-name').html(product.name);
            });

            //Clicking the confirmation button will trigger the following actions
            $('#content').on('click', '#delete-product-confirmation-button', function () {
                deleteProduct(productId);
                productId = null;
            });

            //Deleting from database
            let deleteProduct = function(id){
                $.ajax({
                    url: "{{route('shopkeeper.product.delete')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id': id
                    }
                }).done(function (response) {
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Product variation delete section
            //Clicking the delete button will trigger the following actions
            $('#content').on('click', '#product-variation-delete-button', function () {

                productId = $(this).data('product').id;
                productVariation = $(this).data('variation');

                $('#content').find('#delete-product-variation-name').html(productVariation.name);
            });

            //Clicking the confirmation button will trigger the following actions
            $('#content').on('click', '#delete-product-variation-confirmation-button', function () {
                deleteProductVariation();
            });

            //Updating the database with the changes
            let deleteProductVariation = function(){
                $.ajax({
                    url: "{{route('shopkeeper.productVariation.delete')}}",
                    method: "POST",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'productId': productId,
                        'productVariationId' : productVariation.id
                    }
                }).done(function (response) {
                    console.log(response);
                    refreshPage();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Refreshing the page for new data
            let refreshPage = function () {
                $.ajax({
                    url: "{{route('shopkeeper.product.index')}}",
                    method: 'GET',
                }).done(function (data) {
                    dynamicDom = $("#content", data).html();
                    injectDynamicDom();
                    clearVariables();
                }).fail(function (error) {
                    console.log(error);
                });
            }

            //Clicking the form cancel button will trigger the following actions
            $('#content').on('click', '#form-cancel', function () {
                injectDynamicDom()
                clearVariables();
            });

            //Clicking the form cancel button will trigger the following actions
            $('#temporary-content').on('click', '#form-cancel', function () {
                clearVariables();
                injectDynamicDom()
            });

            //Injecting a dynamic dom
            let injectDynamicDom = function () {
                setTimeout(function () {
                    $('#temporary-content').html('');
                    $('#content').html(dynamicDom);
                    $('#show-variation-' + productId).trigger('click');
                }, 200);
            }

            //Clearing All js variables
            let clearVariables = function () {
                productName = '';
                productCategory = '';
                productSubcategory = '';
                productVariationName = [];
                productVariationQuantity = [0];
                productVariationPrice = [0];
                productVariationInfoIterator = 0;
            }
        });
    </script>
@endsection
