<?php


namespace App\Repositories\Shopkeeper;


use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeRepository
{
    /**
     * @param $request
     * @return mixed
     */
    public function storeEmployee($request)
    {
        return Employee::create([
            'shop_id' => Auth::user()->shop->id,
            'name' => $request->name,
            'still_working' => true,
            'salary' => $request->salary,
            'started_at' => Carbon::now()->format('Y-m-d'),
        ]);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function updateEmployee($request)
    {
        $stillWorking = !($request->ended != null && $request->ended <= Carbon::now()->format('Y-m-d'));

        return Employee::where('id', $request->id)
            ->update([
                'name' => $request->name,
                'salary' => $request->salary,
                'still_working' => $stillWorking,
                'started_at' => $request->started,
                'ended_at' => $request->ended,
            ]);
    }

}
