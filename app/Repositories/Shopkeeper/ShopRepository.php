<?php


namespace App\Repositories\Shopkeeper;


use App\Models\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopRepository
{
    /**
     * @param $request
     * @return mixed
     */
    public function storeShop($request)
    {
        return $shop = Shop::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'size' => $request->size,
            'location' => $request->location,
        ]);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function updateShop($request)
    {
        return Shop::where('id', $request->id)
            ->update([
            'name' => $request->name,
            'size' => $request->size,
            'location' => $request->location,
        ]);
    }
}
