<?php


namespace App\Repositories\Shopkeeper;



use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    /**
     * @param $products
     * @return int
     */
    public function countProductVariations($products)
    {
        $count = 0;
        foreach ($products as $product){
            $count += $product->productVariations->count();
        }

        return $count;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function storeProduct($request)
    {
        return $product = Product::create([
            'shop_id' => Auth::user()->shop->id,
            'category_id' => $request->productCategoryId,
            'subcategory_id' => $request->productSubcategoryId,
            'name' => $request->productName,
        ]);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function updateProduct($request)
    {
        return Product::where('id', $request->id)
            ->update([
                'category_id' => $request->categoryId,
                'subcategory_id' => $request->subcategoryId,
                'name' => $request->name,
            ]);
    }
}
