<?php


namespace App\Repositories\Shopkeeper;


use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Auth;

class FilterRepository
{
    /**
     * @return mixed
     */
    public function getProductVariationMaxQuantity()
    {
        $shopProductIds = Auth::user()->shop->products()->pluck('id');
        return ProductVariation::whereIn('product_id', $shopProductIds)->orderBy('quantity', 'desc')->first()->quantity;
    }

    /**
     * @return mixed
     */
    public function getProductVariationMinQuantity()
    {
        $shopProductIds = Auth::user()->shop->products()->pluck('id');
        return ProductVariation::whereIn('product_id', $shopProductIds)->orderBy('quantity', 'ASC')->first()->quantity;
    }

    /**
     * @return mixed
     */
    public function getProductVariationMaxPrice()
    {
        $shopProductIds = Auth::user()->shop->products()->pluck('id');
        return ProductVariation::whereIn('product_id', $shopProductIds)->orderBy('price', 'desc')->first()->price;
    }

    /**
     * @return mixed
     */
    public function getProductVariationMinPrice()
    {
        $shopProductIds = Auth::user()->shop->products()->pluck('id');
        return ProductVariation::whereIn('product_id', $shopProductIds)->orderBy('price', 'ASC')->first()->price;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function searchProductsByName($name)
    {
        return ProductVariation::select(
            'products.name as productName',
            'product_variations.name as name',
            'product_variations.quantity as quantity',
            'product_variations.price as price',
            'product_variations.status as status'
        )
            ->leftJoin('products', ['product_variations.product_id' => 'products.id'])
            ->leftJoin('categories', ['products.category_id' => 'categories.id'])
            ->leftJoin('subcategories', ['products.subcategory_id' => 'subcategories.id'])
            ->where('products.shop_id', Auth::user()->shop->id)
            ->where(function ($query) use($name){
                $query->where('categories.name', 'like', "%{$name}%")
                    ->orWhere('subcategories.name', 'like', "%{$name}%")
                    ->orWhere('products.name', 'like', "%{$name}%")
                    ->orWhere('product_variations.name', 'like', "%{$name}%");
            })
            ->get();
    }

    /**
     * @param $categoryIds
     * @param $subcategoryIds
     * @param $quantity
     * @param $price
     * @return mixed
     */
    public function searchProductsByOptions($categoryIds, $subcategoryIds, $quantity, $price)
    {
        return ProductVariation::select(
            'products.name as productName',
            'product_variations.name as name',
            'product_variations.quantity as quantity',
            'product_variations.price as price',
            'product_variations.status as status'
        )
            ->leftJoin('products', ['product_variations.product_id' => 'products.id'])
            ->leftJoin('categories', ['products.category_id' => 'categories.id'])
            ->leftJoin('subcategories', ['products.subcategory_id' => 'subcategories.id'])
            ->where('products.shop_id', Auth::user()->shop->id)
            ->whereIn('categories.id', $categoryIds)
            ->whereIn('subcategories.id', $subcategoryIds)
            ->whereBetween('product_variations.quantity', $quantity)
            ->whereBetween('product_variations.price', $price)
            ->get();
    }
}
