<?php


namespace App\Repositories\Shopkeeper;

use App\Models\ProductVariation;
use Illuminate\Support\Facades\DB;

class ProductVariationRepository
{
    /**
     * @param $products
     * @return int
     */
    public function countProductVariations($products)
    {
        $count = 0;
        foreach ($products as $product) {
            $count += $product->productVariations->count();
        }
        return $count;
    }

    /**
     * @param $productVariation
     * @return mixed
     */
    public function storeProductVariation($productVariation)
    {
        return ProductVariation::create($productVariation);
    }

    /**
     * @param $productVariation
     * @param $request
     * @return mixed
     */
    public function updateProductVariation($productVariation, $request)
    {
        $status  = $request->quantity > 0 ? ACTIVE : INACTIVE;

        return $productVariation->update([
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'status' => $status
        ]);
    }
}
