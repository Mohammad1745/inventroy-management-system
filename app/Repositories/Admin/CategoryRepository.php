<?php


namespace App\Repositories\Admin;


use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryRepository
{
    /**
     * @param $request
     * @return bool
     */
    public function storeCategory($request)
    {
        return Category::create([
            'name' => $request->name ,
        ]);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function updateCategory($request)
    {
        return  Category::where('id', $request->id)
            ->update([
                'name' => $request->name,
            ]);
    }
}
