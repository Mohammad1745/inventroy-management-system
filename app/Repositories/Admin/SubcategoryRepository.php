<?php


namespace App\Repositories\Admin;


use App\Models\Subcategory;
use Illuminate\Support\Facades\DB;

class SubcategoryRepository
{
    /**
     * @param $subcategory
     * @return mixed
     */
    public function storeSubcategory($subcategory)
    {
        return Subcategory::create($subcategory);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function updateSubcategory($request)
    {
        return Subcategory::where('id', $request->id)
            ->update([
                'name' => $request->name,
            ]);
    }
}
