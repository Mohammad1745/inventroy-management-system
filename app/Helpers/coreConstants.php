<?php
//user role
const ADMIN = 1;
const SHOPKEEPER = 2;

//shop size
const SMALL = 1;
const MEDIUM = 2;
const LARGE = 3;
const EXTRA_LARGE = 4;

//product status
const INACTIVE = 1;
const ACTIVE = 2;


const NOT_STARTED = 1;
const RUNNING = 2;
const COMPLETED = 3;
