<?php


namespace App\Services\Admin;


use App\Models\Category;
use App\Models\Subcategory;
use App\Repositories\Admin\SubcategoryRepository;

class SubcategoryService
{
    private $subcategoryRepository;

    /**
     * SubcategoryService constructor.
     */
    public function __construct()
    {
        $this->subcategoryRepository = new SubcategoryRepository();
    }

    public function storeSubcategory($request)
    {
        $categoryExists =Category::where('id', $request->categoryId)->exists();
        if($categoryExists) {
            try{
                $name = $request->subcategoryName;
                $subcategories = [];
                for($i=0; $i<sizeof($name); $i++){
                    $subcategories[$i] = $this->subcategoryRepository->storeSubcategory([
                        'category_id' => $request->categoryId,
                        'name' => $name[$i],
                    ]);
                }
                return [
                    'success' => true,
                    'data' => $subcategories,
                    'message' => 'Successful'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => true,
                'data' => null,
                'message' => "Category Not Found!"
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateSubcategory($request)
    {
        $subcategoryExists = Subcategory::where('id', $request->id)->exists();

        if($subcategoryExists){
            try{
                $subcategory = $this->subcategoryRepository->updateSubcategory($request);

                return [
                    'success' => true,
                    'data' => $subcategory,
                    'message' => 'Successful'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Subcategory not found'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteSubcategory($request)
    {
        $isDeleted = Subcategory::where('id', $request->id)->first()->delete();
        if($isDeleted){
            return [
                'success' => true,
                'message' => 'Subcategory has been deleted successfully.'
            ];
        }
        else{
            return [
                'success' => false,
                'message' => 'Subcategory cannot be deleted!'
            ];
        }
    }
}
