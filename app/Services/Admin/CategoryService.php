<?php


namespace App\Services\Admin;


use App\Models\Category;
use App\Repositories\Admin\CategoryRepository;

class CategoryService
{
    private $categoryRepository;

    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function storeCategory($request)
    {
        $category = Category::where('name', $request->name)->first();
        if($category == null){
            try{
                $category = $this->categoryRepository->storeCategory($request);

                return [
                    'success' => true,
                    'data' => $category,
                    'message' => 'Successful'
                ];

            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => true,
                'data' => $category,
                'message' => "Already Exists"
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateCategory($request)
    {
        $categoryExists = Category::where('id', $request->id)->exists();

        if($categoryExists){
            try{
                $category = $this->categoryRepository->updateCategory($request);

                return [
                    'success' => true,
                    'data' => $category,
                    'message' => 'Successful'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Category not found'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteCategory($request)
    {
        $isDeleted = Category::where('id', $request->id)->first()->delete();
        if($isDeleted){
            return [
                'success' => true,
                'message' => 'Category has been deleted successfully.'
            ];
        }
        else{
            return [
                'success' => false,
                'message' => 'Category cannot be deleted!'
            ];
        }
    }
}
