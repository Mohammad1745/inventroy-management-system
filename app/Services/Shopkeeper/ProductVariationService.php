<?php


namespace App\Services\Shopkeeper;


use App\Models\ProductVariation;
use App\Repositories\Shopkeeper\ProductVariationRepository;
use Illuminate\Support\Facades\Auth;

class ProductVariationService
{
    private $productVariationRepository;

    /**
     * ProductVariationService constructor.
     */
    public function __construct()
    {
        $this->productVariationRepository = new ProductVariationRepository();
    }

    /**
     * @return int
     */
    public function countProductVariations()
    {
        $count=0;
        $products = Auth::user()->shop->products;
        foreach ($products as $product){
            $count += $product->productVariations()->count();
        }

        return $count;
    }

    /**
     * @param $request
     * @return array
     */
    public function storeProductVariation($request)
    {
        $productExists = Auth::user()->shop->products()->where('id', $request->productId)->exists();
        if($productExists) {
            try{
                $name = $request->productVariationName;
                $quantity = $request->productVariationQuantity;
                $price = $request->productVariationPrice;
                $productVariations = [];
                for($i=0; $i<sizeof($name); $i++){
                    $status = $quantity[$i] > 0 ? ACTIVE : INACTIVE;
                    $productVariations[$i] = $this->productVariationRepository->storeProductVariation([
                        'product_id' => $request->productId,
                        'name' => $name[$i],
                        'quantity' => $quantity[$i],
                        'price' => $price[$i],
                        'status' => $status,
                    ]);
                }
                return [
                    'success' => false,
                    'data' => $productVariations,
                    'message' => 'Product variation have been created successfully.'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => true,
                'data' => null,
                'message' => "Product Not Found!"
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProductVariation($request)
    {
        $productVariation = ProductVariation::find($request->id);
        if($productVariation != null){
            try{
                $this->productVariationRepository->updateProductVariation($productVariation, $request);
                $productVariation = Auth::user()->shop->products()->where('id', $request->id)->first();

                return [
                    'success' => true,
                    'data' => $productVariation,
                    'message' => 'Product variation has been added successfully.'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Product variation not found!'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteProductVariation($request)
    {
        $isDeleted = Auth::user()->shop
            ->products()->where('id', $request->productId)->first()
            ->productVariations()->where('id', $request->productVariationId)->first()
            ->delete();
        if ($isDeleted) {
            return [
                'success' => true,
                'message' => 'Product variation has been deleted successfully.'
            ];
        }
        else {
            return [
                'success' => false,
                'message' => 'Product variation cannot be deleted!'
            ];
        }
    }
}
