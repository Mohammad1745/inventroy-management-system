<?php


namespace App\Services\Shopkeeper;


use App\Models\Employee;
use App\Repositories\Shopkeeper\EmployeeRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EmployeeService
{
    private $employeeRepository;

    /**
     * EmployeeService constructor.
     */
    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function storeEmployee($request)
    {
        try {
            $employee = $this->employeeRepository->storeEmployee($request);

            return [
                'success' => true,
                'data' => $employee,
                'message' => 'Successful'
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'data' => null,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateEmployee($request)
    {
        $employeeExists = Auth::user()->shop->employees()->where('id', $request->id)->exists();

        if ($employeeExists) {
            try {
                $employee = $this->employeeRepository->updateEmployee($request);

                return [
                    'success' => true,
                    'data' => $employee,
                    'message' => 'Successful'
                ];
            } catch (\Exception $e) {
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        } else {
            return [
                'success' => false,
                'data' => null,
                'message' => 'Employee not found'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteEmployee($request)
    {
        $isDeleted = Employee::where('id', $request->id)->first()->delete();
        if ($isDeleted) {
            return [
                'success' => true,
                'message' => 'Employee has been deleted successfully.'
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Employee cannot be deleted!'
            ];
        }
    }
}
