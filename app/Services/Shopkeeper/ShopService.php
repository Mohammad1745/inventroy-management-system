<?php


namespace App\Services\Shopkeeper;


use App\Models\Shop;
use App\Repositories\Shopkeeper\ShopRepository;
use http\Client\Curl\User;
use Illuminate\Support\Facades\Auth;

class ShopService
{

    private $shopRepository;

    /**
     * ShopService constructor.
     */
    public function __construct()
    {
        $this->shopRepository = new ShopRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function storeShop($request)
    {
        $shop = Shop::where('user_id', Auth::user()->id)->first();
        if($shop == null){
            try{
                $shop = $this->shopRepository->storeShop($request);

                return [
                    'success' => true,
                    'data' => $shop,
                    'message' => 'Shop has been created successfully.'
                ];

            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => true,
                'data' => $shop,
                'message' => "Shop already Exists"
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateShop($request)
    {
        $shopExists = Auth::user()->shop->where('id', $request->id)->exists();
        if($shopExists){
            try{
                $this->shopRepository->updateShop($request);
                $shop = Shop::find($request->id);
                $shop['sizeInString'] = $shop->size();

                return [
                    'success' => true,
                    'data' => $shop,
                    'message' => 'Successful'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Shop not found'
            ];
        }
    }
}
