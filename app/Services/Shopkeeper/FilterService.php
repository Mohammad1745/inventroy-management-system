<?php


namespace App\Services\Shopkeeper;


use App\Models\Category;
use App\Models\Subcategory;
use App\Repositories\Shopkeeper\FilterRepository;
use Illuminate\Support\Facades\Auth;

class FilterService
{
    private $filterRepository;

    /**
     * FilterService constructor.
     */
    public function __construct()
    {
        $this->filterRepository = new FilterRepository();
    }

    /**
     * @return mixed
     */
    public function getProductVariationMaxQuantity()
    {
        return $this->filterRepository->getProductVariationMaxQuantity();
    }

    /**
     * @return mixed
     */
    public function getProductVariationMinQuantity()
    {
        return $this->filterRepository->getProductVariationMinQuantity();
    }

    /**
     * @return mixed
     */
    public function getProductVariationMaxPrice()
    {
        return $this->filterRepository->getProductVariationMaxPrice();
    }

    /**
     * @return mixed
     */
    public function getProductVariationMinPrice()
    {
        return $this->filterRepository->getProductVariationMinPrice();
    }

    /**
     * @param $request
     * @return array
     */
    public function filterProductsByName($request)
    {
        $name = $request->name;
        if($name!=null){
            try{
                $products = $this->filterRepository->searchProductsByName($name);
                if(sizeof($products)>0){
                    return [
                        'success' => true,
                        'data' => $products,
                        'message' => 'Product found'
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'data' => null,
                        'message' => 'No Product Found'
                    ];
                }
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => 'Something went wrong!'
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Empty Request'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function filterProductsByOptions($request)
    {
        $categoryIds = $request->categoryIds;
        $subcategoryIds = $request->subcategoryIds;
        $quantity = $request->quantity;
        $price = $request->price;
        if ($categoryIds == null){
            $categoryIds = Category::pluck('id');
            $subcategoryIds = Subcategory::pluck('id');
        }
        elseif ($subcategoryIds == null){
            $subcategoryIds = Subcategory::whereIn('category_id', $categoryIds)->pluck('id');
        }
        try{
            $products = $this->filterRepository->searchProductsByOptions($categoryIds, $subcategoryIds, $quantity, $price);
            if(sizeof($products)>0){
                return [
                    'success' => true,
                    'data' => $products,
                    'message' => 'Product found'
                ];
            }
            else{
                return [
                    'success' => false,
                    'data' => null,
                    'message' => 'No Product Found'
                ];
            }
        }catch (\Exception $e){
            return [
                'success' => false,
                'data' => null,
                'message' => 'Something went wrong!'
            ];
        }
    }
}
