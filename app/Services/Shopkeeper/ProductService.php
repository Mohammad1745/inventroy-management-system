<?php


namespace App\Services\Shopkeeper;

use App\Repositories\Shopkeeper\ProductRepository;
use Illuminate\Support\Facades\Auth;

class ProductService
{
    private $productRepository;

    /**
     * ProductService constructor.
     */
    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    /**
     * @return int
     */
    public function countProductVariations()
    {
        $count=0;
        $products = Auth::user()->shop->products;

        foreach ($products as $product){
            $count += $product->productVariations()->count();
        }

        return $count;
    }

    /**
     * @param $request
     * @return array
     */
    public function storeProduct($request)
    {
        $product = Auth::user()->shop->products()->where('name', $request->productName)->first();
        if($product == null){
            try{
                $product = $this->productRepository->storeProduct($request);

                return [
                    'success' => true,
                    'data' => $product,
                    'message' => 'Product has been created successfully.'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => true,
                'data' => $product,
                'message' => "Product already Exists"
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProduct($request)
    {
        $productExists = Auth::user()->shop->products()->where('id', $request->id)->exists();
        if($productExists){
            try{
                $this->productRepository->updateProduct($request);
                $product = Auth::user()->shop->products()->where('id', $request->id)->first();

                return [
                    'success' => true,
                    'data' => $product,
                    'message' => 'Product has been added successfully.'
                ];
            }catch (\Exception $e){
                return [
                    'success' => false,
                    'data' => null,
                    'message' => $e->getMessage()
                ];
            }
        }
        else{
            return [
                'success' => false,
                'data' => null,
                'message' => 'Product not found!'
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteProduct($request)
    {
        $isDeleted =  Auth::user()->shop->products()->where('id', $request->id)->first()->delete();
        if ($isDeleted) {
            return [
                'success' => true,
                'message' => 'Product has been deleted successfully.'
            ];
        }
        else {
            return [
                'success' => false,
                'message' => 'Product cannot be deleted!'
            ];
        }
    }
}
