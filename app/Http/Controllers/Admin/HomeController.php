<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Employee;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Shop;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function  __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shopAmount = Shop::all()->count();
        $productAmount = Product::all()->count();
        $productVariationAmount = ProductVariation::all()->count();
        $employeeAmount = Employee::all()->count();
        $categoryAmount = Category::all()->count();
        $subcategoryAmount = Subcategory::all()->count();

        return view('admin.home', compact('shopAmount', 'productAmount', 'productVariationAmount', 'employeeAmount', 'categoryAmount', 'subcategoryAmount'));
    }
}
