<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SubcategoryRequest;
use App\Models\Subcategory;
use App\Services\Admin\SubcategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoryController extends Controller
{
    private $subcategoryService;

    /**
     * SubcategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->subcategoryService = new SubcategoryService();
    }

    /**
     * @param SubcategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SubcategoryRequest $request)
    {
        $response = $this->subcategoryService->storeSubcategory($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $response = $this->subcategoryService->updateSubcategory($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = $this->subcategoryService->deleteSubcategory($request);

        return response()->json($response);
    }

}
