<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Services\Shopkeeper\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    private $productService;

    /**
     * HomeController constructor.
     */
    public function  __construct()
    {
        $this->middleware('auth');
        $this->middleware('shopkeeper');
        $this->productService = new ProductService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shop = Auth::user()->shop;
        $productAmount = $shop->products()->count();
        $productVariationAmount = $this->productService->countProductVariations();
        $employeeAmount = $shop->employees()->count();
        return view('shopkeeper.home', compact('shop', 'productAmount', 'productVariationAmount', 'employeeAmount'));
    }
}
