<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Http\Requests\Shopkeeper\EmployeeRequest;
use App\Models\Employee;
use App\Services\Shopkeeper\EmployeeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    private $employeeService;

    /**
     * EmployeeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('shopkeeper');
        $this->employeeService = new EmployeeService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shop = Auth::user()->shop;
        $employees = $shop->employees;
        return view('shopkeeper.employee.index', compact('shop', 'employees'));
    }

    /**
     * @param EmployeeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EmployeeRequest $request)
    {
        $response = $this->employeeService->storeEmployee($request);

        return response()->json($response);
    }

    /**
     * @param EmployeeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EmployeeRequest $request)
    {
        $response = $this->employeeService->updateEmployee($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = $this->employeeService->deleteEmployee($request);

        return response()->json($response);
    }

}
