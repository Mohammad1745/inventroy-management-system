<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Http\Requests\Shopkeeper\ProductRequest;
use App\Http\Requests\Shopkeeper\ProductVariationRequest;
use App\Models\Category;
use App\Models\Subcategory;
use App\Services\Shopkeeper\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use test\Mockery\ReturnTypeObjectTypeHint;

class ProductController extends Controller
{
    private $productService;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('shopkeeper');
        $this->productService = new ProductService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shop = Auth::user()->shop;
        $products =$shop->products;
        $productAmount =$products->count();
        $categories = Category::all();
        $subcategories = Subcategory::all();

        return view('shopkeeper.product.index', compact(  'shop','products', 'productAmount', 'categories', 'subcategories'));
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductRequest $request)
    {
        $response = $this->productService->storeProduct($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $response = $this->productService->updateProduct($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = $this->productService->deleteProduct($request);

        return response()->json($response);
    }
}
