<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Models\Category;
use App\Models\Subcategory;
use App\Services\Shopkeeper\FilterService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FilterController extends Controller
{
    private $filterService;

    /**
     * FilterController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('shopkeeper');
        $this->filterService = new FilterService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shop = Auth::user()->shop;
        return view('shopkeeper.filter.index', compact('shop'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function initialInformation()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $productVariationMaxQuantity = $this->filterService->getProductVariationMaxQuantity();
        $productVariationMinQuantity = $this->filterService->getProductVariationMinQuantity();
        $productVariationMaxPrice = $this->filterService->getProductVariationMaxPrice();
        $productVariationMinPrice = $this->filterService->getProductVariationMinPrice();

        return response()->json([
           'categories' => $categories,
           'subcategories' => $subcategories,
           'maxQuantity' => $productVariationMaxQuantity,
           'minQuantity' => $productVariationMinQuantity,
           'maxPrice' => $productVariationMaxPrice,
           'minPrice' => $productVariationMinPrice,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterByName(Request $request)
    {
        $response = $this->filterService->filterProductsByName($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterByOptions(Request $request)
    {
        $response = $this->filterService->filterProductsByOptions($request);

        return response()->json($response);
    }
}
