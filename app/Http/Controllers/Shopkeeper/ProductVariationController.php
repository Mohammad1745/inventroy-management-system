<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Http\Requests\Shopkeeper\ProductVariationRequest;
use App\Services\Shopkeeper\ProductVariationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductVariationController extends Controller
{
    private $productVariationService;

    /**
     * ProductVariationController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('shopkeeper');
        $this->productVariationService = new ProductVariationService();
    }

    /**
     * @param ProductVariationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductVariationRequest $request)
    {
        $response = $this->productVariationService->storeProductVariation($request);

        if($response){
            return response()->json([
                'success' => 'Product Variation has been updated Successfully!'
            ]);
        }
        //else
        return response()->json([
            'error' => 'Product Variation cannot be updated!'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $response = $this->productVariationService->updateProductVariation($request);

        if($response){
            return response()->json([
                'success' => 'Product Variation has been updated Successfully!'
            ]);
        }
        //else
        return response()->json([
            'error' => 'Product Variation cannot be updated!'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = $this->productVariationService->deleteProductVariation($request);

        return response()->json($response);
    }
}
