<?php

namespace App\Http\Controllers\Shopkeeper;

use App\Http\Requests\Shopkeeper\ShopRequest;
use App\Models\Shop;
use App\Services\Shopkeeper\ShopService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    private $shopService;

    /**
     * ShopController constructor.
     */
    public function __construct()
    {
        $this->shopService = new ShopService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $shop = new Shop(); //Blank Object

        return view('shopkeeper.shop.create', compact('shop'));
    }

    /**
     * @param ShopRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ShopRequest $request)
    {
        $response = $this->shopService->storeShop($request);
        if ($response['success']){
            return redirect(route('shopkeeper.home'))->with('success', $response['message']);
        }
        return redirect()->back()->with('error', $response['message']);
    }

    /**
     * @param ShopRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ShopRequest $request)
    {
        $response = $this->shopService->updateShop($request);

        return response()->json($response);
    }
}
