<?php

namespace App\Http\Middleware;

use App\Models\Shop;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckShopkeeper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRole = Auth::user()->role;

        if($userRole == SHOPKEEPER){

            $shopExists = Shop::where('user_id', Auth::user()->id)->exists();

            if ($shopExists == false){
                return redirect(route('shopkeeper.shop.create'));
            }
            return $next($request);
        }
        elseif($userRole == ADMIN){
            return redirect(route('admin.home'));
        }
        else{
            return redirect(url('/'));
        }
    }
}
