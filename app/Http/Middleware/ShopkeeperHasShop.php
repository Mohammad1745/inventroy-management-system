<?php

namespace App\Http\Middleware;

use App\Models\Shop;
use Closure;
use Illuminate\Support\Facades\Auth;

class ShopkeeperHasShop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role == ADMIN) return redirect(route('admin.home'));

        //else
        $shopExists = Shop::where('user_id', Auth::user()->id)->exists();

        if ($shopExists == true){
            return redirect(route('shopkeeper.home'))->with('info', 'You have already built your shop!');
        }
        return $next($request);
    }
}
