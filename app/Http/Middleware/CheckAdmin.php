<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRole = Auth::user()->role;
        if($userRole == ADMIN){
            return $next($request);
        }
        elseif($userRole == SHOPKEEPER){
            return redirect(route('shopkeeper.home'));
        }
        else{
            return redirect(url('/'));
        }
    }
}
