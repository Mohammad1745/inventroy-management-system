<?php

namespace App\Http\Requests\Shopkeeper;

use Illuminate\Foundation\Http\FormRequest;

class ProductVariationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productId' => 'required',
            'productVariationName' => 'required',
            'productVariationQuantity' => 'required',
            'productVariationPrice' => 'required',
        ];
    }
}
