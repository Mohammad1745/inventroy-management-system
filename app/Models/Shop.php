<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = ['user_id', 'name', 'size', 'location'];

    /**
     * @return array
     */
    public function sizeOptions()
    {
        return [
            SMALL => 'Small',
            MEDIUM => 'Medium',
            LARGE => "Large",
            EXTRA_LARGE => 'Extra Large',
        ];
    }

    /**
     * @return string
     */
    public function size()
    {
        if($this->size == SMALL) return 'Small';
        elseif($this->size == MEDIUM) return 'Medium';
        elseif($this->size == LARGE) return 'Large';
        elseif($this->size == EXTRA_LARGE) return 'Extra Large';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products(){
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees(){
        return $this->hasMany(Employee::class);
    }
}
