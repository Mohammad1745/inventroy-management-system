<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::namespace('Admin')->group(function () {
    Route::get('/admin', 'HomeController@index')->name('admin.home');

    Route::get('/admin/category/details', 'CategoryController@details')->name('admin.category.details');
    Route::post('/admin/category/store', 'CategoryController@store')->name('admin.category.store');
    Route::post('/admin/category/update', 'CategoryController@update')->name('admin.category.update');
    Route::post('/admin/category/delete', 'CategoryController@destroy')->name('admin.category.delete');

    Route::post('/admin/subcategory/store', 'SubcategoryController@store')->name('admin.subcategory.store');
    Route::post('/admin/subcategory/update', 'SubcategoryController@update')->name('admin.subcategory.update');
    Route::post('/admin/subcategory/delete', 'SubcategoryController@destroy')->name('admin.subcategory.delete');
});

Route::namespace('Shopkeeper')->group(function () {
    Route::get('/shopkeeper', 'HomeController@index')->name('shopkeeper.home');

    Route::get('/shopkeeper/shop/create', 'ShopController@create')->name('shopkeeper.shop.create')->middleware('shop');//if shop exists, these
    Route::post('/shopkeeper/shop/store', 'ShopController@store')->name('shopkeeper.shop.store')->middleware('shop');  //routes will be redirected.
    Route::post('/shopkeeper/shop/update', 'ShopController@update')->name('shopkeeper.shop.update');

    Route::get('/shopkeeper/employee', 'EmployeeController@index')->name('shopkeeper.employee.index');
    Route::post('/shopkeeper/employee/store', 'EmployeeController@store')->name('shopkeeper.employee.store');
    Route::post('/shopkeeper/employee/update', 'EmployeeController@update')->name('shopkeeper.employee.update');
    Route::post('/shopkeeper/employee/delete', 'EmployeeController@destroy')->name('shopkeeper.employee.delete');

    Route::get('/shopkeeper/product', 'ProductController@index')->name('shopkeeper.product.index');
    Route::post('/shopkeeper/product/store', 'ProductController@store')->name('shopkeeper.product.store');
    Route::post('/shopkeeper/product/update', 'ProductController@update')->name('shopkeeper.product.update');
    Route::post('/shopkeeper/product/delete', 'ProductController@destroy')->name('shopkeeper.product.delete');

    Route::post('/shopkeeper/product-variation/store', 'ProductVariationController@store')->name('shopkeeper.productVariation.store');
    Route::post('/shopkeeper/product-variation/update', 'ProductVariationController@update')->name('shopkeeper.productVariation.update');
    Route::post('/shopkeeper/product-variation/delete', 'ProductVariationController@destroy')->name('shopkeeper.productVariation.delete');

    Route::get('/shopkeeper/filter', 'FilterController@index')->name('shopkeeper.filter.index');
    Route::get('/shopkeeper/filter/initial-information', 'FilterController@initialInformation')->name('shopkeeper.filter.initialInformation');
    Route::post('/shopkeeper/filter-by-name', 'FilterController@filterByName')->name('shopkeeper.filterByName');
    Route::post('/shopkeeper/filter-by-options', 'FilterController@filterByOptions')->name('shopkeeper.filterByOptions');
});
